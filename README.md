# Tour

This is a multilanguage repository, you can find mixed information in catalan, english and spanish

what can you find here?

- [basic](./basic): essential information for all community network users

- [howto](./howto): technical articles with a very pragmatic approach about different topics related to guifi (spectrum, wifi, systems, protocols, etc.)

- [info](./info): general documentation of relevant things for community networks

- [presentations](./presentations): some of the recent presentations, we should move it to another place

# LFS

[LFS](https://gitlab.com/help/workflow/lfs/manage_large_binaries_with_git_lfs) is a system to track heavy files in git repositories. Also I see it as a way to separate text files from other/media sources.

If you just `git clone` (without `apt install git-lfs`) you are going to be missing [this kind of files](./gitattributes)

If you have git lfs installed and you don't want to download media files do it like: `GIT_LFS_SKIP_SMUDGE=1 git clone (...)` src https://github.com/git-lfs/git-lfs/issues/2406

# Invisible credits

Sometimes the commits are not done by the original authors

- Josep Mercader
- Ramon Selga
- Thijs
- Matteo
- Ivan Vilata
- Al

# License

[LICENSE](./LICENSE) refers to default license (if not specified)
