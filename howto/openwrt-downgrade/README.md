to safely put openwrt you have to downgrade firmware

| product | link | sha256sum | comments |
| ------- | --------- | --------- | -------- |
| XM series | [link](XM.v5.5.11.28002.150723.1344.bin) | bfc85c68b16c760b112f763ade4e6c3c4ac49c7e1f46436fd67ec7b911719600 | tested |
| XW series | [link](XW.v5.5.10-u2.28005.150723.1358.bin) | b222ae2fccd3a108cfc170197ca9b588048edf137846891b71ec863995bb7954 | tested |
| ? | [link](TI.v5.5.11.28002.150723.1518.bin) | 201e024b081d6b828822ada56c4282d40bb8ff1b9489442ff94c2ece046129c4 | ? |
| AC Lite | [link](UniFi-firmware-3.7.58-for-UAP-AC-Lite-LR-Pro-EDU-M-M-PRO-IW-IW-Pro__BZ.qca956x.v3.7.58.6385.170508.0957__2017-05-15.bin) | cfe2e639c430d1a849c2892effb84157e82ea9cb08d174547c1e001d4d4388a8 | we are going to test it. Modified original filename (substring between `__`). More info https://openwrt.org/toh/ubiquiti/unifiac |
