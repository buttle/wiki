

  #######################################################################################
  #                                                                                     #
  #             _     _                                  _           _                  #
  #             | |__ (_) __ _  ___ _ __ _   _ _ __   ___| |__    ___| |__              #
  #             | '_ \| |/ _` |/ __| '__| | | | '_ \ / __| '_ \  / __| '_ \             #
  #             | |_) | | (_| | (__| |  | |_| | | | | (__| | | |_\__ \ | | |            #
  #             |_.__/|_|\__, |\___|_|   \__,_|_| |_|\___|_| |_(_)___/_| |_|            #
  #                      |___/                                                          #
  #                                                                                     #
  #                                                                                     #
  #    non interactive way to reinstall a clean slapd with the configuration we want    #
  #                                                                                     #
  #######################################################################################


password="admin"
organization="exo.cat"

# src https://unix.stackexchange.com/questions/362547/automating-slapd-install
#   see other variables with: debconf-show slapd
cat > debconf-slapd.conf <<EOF
slapd slapd/internal/generated_adminpw password $password
slapd slapd/password1 password $password
slapd slapd/password2 password $password
slapd slapd/internal/adminpw password $password
slapd slapd/ppolicy_schema_needs_update select abort installation
slapd shared/organization string $organization
slapd slapd/move_old_database boolean true
slapd slapd/backend select MDB
slapd slapd/domain string $organization
slapd slapd/invalid_config boolean true
slapd slapd/dump_database select when needed
slapd slapd/purge_database boolean true
slapd slapd/no_configuration boolean false
slapd slapd/dump_database_destdir string /var/backups/slapd-VERSION
EOF
export DEBIAN_FRONTEND=noninteractive
cat debconf-slapd.conf | debconf-set-selections
# orig
#rm -rf /var/backups/unknown-2.4.44+dfsg-5+deb9u2.ldapdb/
# my strange thing
rm -rf /var/backups/dc=exo,dc=cat-2.4.44+dfsg-5+deb9u2.ldapdb

# cleans DB and starts again
dpkg-reconfigure slapd
apt purge slapd -y
apt install slapd -y
