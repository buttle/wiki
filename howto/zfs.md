# install

if you are running debian you require stretch-backports:

    apt-get -t stretch-backports install zfs-dkms spl-dkms zfsutils-linux libnvpair1linux libuutil1linux libzfs2linux zfs-zed zfsutils-linux libzpool2linux spl

if you are running proxmox:

    apt-get install zfsutils zfs-initramfs
