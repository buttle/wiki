Based on the debian experience. Debian uses 3 types of bug tracking systems: (1) [debbugs](https://en.wikipedia.org/wiki/Debbugs), (2) its [gitlab](https://gitlab.com) [instance](https://salsa.debian.org/public) and (3) [request tracker (rt)](https://bestpractical.com/request-tracker/) [instance](https://rt.debian.org/). debbugs and gitlab are more convenient for software issues and rt for service desk support. This guide explains how to install it for our organization.

This guide assumes you installed a mail server that you control (through a postfix aliases mechanism). Building mail server is complex but you worth it. At the moment we don't have a guide for that. [Decide what email system to use](https://rt-wiki.bestpractical.com/wiki/EmailInterface) and adapt this guide to your needs.

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**

- [Install](#install)
- [Configure](#configure)
  - [Problems and details about ResetPassword](#problems-and-details-about-resetpassword)
  - [TODO](#todo)
- [Design & Customizations](#design--customizations)
  - [Proposed generic structure](#proposed-generic-structure)
  - [Notify AdminCc when ticket arrives to your queue](#notify-admincc-when-ticket-arrives-to-your-queue)
  - [Autodispatcher to specific queue](#autodispatcher-to-specific-queue)
  - [Change status (lifecycles)](#change-status-lifecycles)
  - [Templates in catalan](#templates-in-catalan)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Install

The steps described [here](https://rt-wiki.bestpractical.com/wiki/DebianJessieInstallGuide) works too for debian stretch. This should deal with installation of all necessary apache2 packages follow the configuration for the database setup and rt root password:

    apt-get install request-tracker4 rt4-db-mysql rt4-apache2

Install extension to allow commands by email (reference https://metacpan.org/pod/RT::Extension::CommandByMail)

    cpan YAML
    cpan install
    cpan RT::Extension::CommandByMail

Install extension to allow reset password method (reference: https://metacpan.org/pod/RT::Extension::ResetPassword)

    cpan RT::Extension::ResetPassword

# Configure

Put the rt config to apache

    cd /etc/apache2/conf-enabled/
    ln -s /etc/request-tracker4/apache2-modperl2.conf

Adapt configuration to our needs in `/etc/request-tracker4/RT_SiteConfig.d/50-debconf.pm`:

    # THE BASICS:

    Set($rtname, 'example.com');
    Set($Organization, 'orgexample');
    Set($Timezone, 'Europe/Madrid');

    Set($CorrespondAddress , 'rt@example.com');
    Set($CommentAddress , 'rt-comment@example.com');

    # THE WEBSERVER:

    Set($WebDomain, 'example.com');
    Set($WebBaseURL , "https://example.com");
    Set($WebPath , "/rt");

    #Set(@ReferrerWhitelist, qw(example.com:443, example.com:80));
    #lets try this -> src https://forum.bestpractical.com/t/cross-site-forgery/31763
    Set(@ReferrerWhitelist, qw());

To create, reply and comment tickets through email with rt put in `/etc/aliases`:

    suport: "|/usr/bin/rt-mailgate --queue suport --action correspond --url http://localhost/rt"
    suport-comentari: "|/usr/bin/rt-mailgate --queue suport --action comment --url http://localhost/rt"

and update aliases appropiately:

    newaliases && service postfix reload

To configure and enable the extensions that allow commands by email (change queue, set status, etc.) plus the extension for password reset you have to include the next three lines in `/etc/request-tracker4/RT_SiteConfig.pm`:

    Plugin('RT::Extension::CommandByMail');
    Set(@MailPlugins, qw(Auth::MailFrom Action::CommandByMail));
    Set($CommandByMailHeader, "X-RT-Command");

    Plugin('RT::Extension::ResetPassword');

Apply all configurations

    service apache2 reload

Configure RT-Shredder (ready to delete things) after that you can access it through: Admin / Tools / Shredder

    mkdir -p /var/lib/request-tracker4/data/RT-Shredder
    chown www-data -R /var/lib/request-tracker4/data/RT-Shredder

## Problems and details about ResetPassword

I reached this error in `/var/log/request-tracker4/rt.log`

> Couldn't load template 'PasswordReset' (/usr/local/share/request-tracker4/plugins/RT-Extension-ResetPassword/html/NoAuth/ResetPassword/Request.html:90)

if we inspect Templates in the web interface (example.com/rt/Admin/Global/Templates.html), it is not there the *PasswordReset: Send user an password reset token*. It could probably work just adding it, but I tried to do it in another way, using the `make initdb` (that it is just a command that looks like just adds the template). To do that I hit an error that assumed that the database admin was postgres in a mysql database (?)

Override that variable to root (in case you are using mysql/mariadb as database) `/etc/request-tracker4/RT_SiteConfig.pm`:

    Set($DatabaseAdmin, 'root');

restart service

    service apache2 restart

run `make initdb`:

    cd /root/.cpan/build/RT-Extension-ResetPassword-1.04-hspHo_
    make initdb

in default's mariadb database for debian just press enter in password prompt

remember that if you want to introduce a user, you have to setup a random password so it can get one. If you try an autogenerated user to reset password it will say:

> You can't reset your password as you don't already have one.

## TODO

- Inmediate TODO (very useful), generate time worked reports https://metacpan.org/pod/RT::Extension::TimeWorkedReport (this is also important reference to have https://rt-wiki.bestpractical.com/wiki/TimeWorkedReport)
- Inmediate TODO [finish integration of the form](https://gitlab.com/guifi-exo/noc-forms) to avoid spam comming to the target email
- Rt can also handle [service level agreements](https://bestpractical.com/blog/2017/3/managing-service-level-agreements-slas-in-rt); but this part is not included in the guide (yet).
- https://rt-wiki.bestpractical.com/wiki/SpamFiltering
- better authentication https://docs.bestpractical.com/rt/4.4.1/authentication.html

# Design & Customizations

## Proposed generic structure

- Each queue handle different types of tickets.
- One queue (the dispatcher) receives all emails and its purpose is to route the tickets to the other queues.
- On each queue specific users and groups receive notifications because they are in the AdminCc

## Notify AdminCc when ticket arrives to your queue

source http://kb.mit.edu/confluence/display/istcontrib/Enabling+notification+to+your+team+when+a+ticket+is+moved+to+your+RT+queue

backup source https://web.archive.org/web/20190218160302/http://kb.mit.edu/confluence/display/istcontrib/Enabling+notification+to+your+team+when+a+ticket+is+moved+to+your+RT+queue

## Autodispatcher to specific queue

```
Condition: On Create
Action: User Defined
Template: Blank
Custom condition:
Custom action preparation code: `return 1;`
Custom action commit code:
```

Assuming that queueName in subject goes automatically to queueName queue:

```perl
# src https://forum.bestpractical.com/t/auto-change-queue-based-on-requestor/15206/3
# src https://rt-wiki.bestpractical.com/wiki/SetOwnerAndQueueBySubject

if (
 $self->TicketObj->Status  eq "new" &&
 $self->TicketObj->Queue   == 4 &&         # ID of suport queue
 # case insensitive https://stackoverflow.com/questions/9655164/regex-ignore-case-sensitivity
 $self->TicketObj->Subject =~ /queueName/i ) {
  $RT::Logger->error("change to alta queue");
  # thanks https://forum.bestpractical.com/t/simple-custom-action-gives-mason-compiler-error/4856/2
  my ($status, $msg) = $self->TicketObj->SetQueue("queueName");
  return 0;
}
```

## Change status (lifecycles)

https://docs.bestpractical.com/rt/4.4.4/customizing/lifecycles.html

## Templates in catalan

(using just the templates that are enabled by default)

Autoreply in HTML - HTML Autoresponse template:

```
Subject: Autoresposta: {$Ticket->Subject}
Content-Type: text/html

<p>Hola,</p>

<p>Aquesta és una resposta automàtica generada en resposta a la teva petició que has titulat <b>{$Ticket->Subject()}</b>,
el teu resum del contingut que s'ha rebut es troba a baix.</p>

<p>No necessites respondre a aquest missatge de moment. El teu ticket se li ha assingat l'identificador <b>{$Ticket->SubjectTag}</b>.</p>

<p>En cas que vulguis respondre, respon a aquest correu i/o següents i incorpora <b>{$Ticket->SubjectTag}</b> com a títol en el email per a totes les futures comunicacions relacionades amb aquest ticket.</p>

<p>SAX,<br/>
{$Ticket->QueueObj->CorrespondAddress()}</p>

<hr/>
{$Transaction->Content(Type => 'text/html')}
```

Resolved in HTML (catalan)

```
Subject: Resolved: {$Ticket->Subject}
Content-Type: text/html

<p>Segons ens consta el teu ticket s'ha resolt. Si tens alguna pregunta més referent a aquest ticket, respon a aquest mail.</p>
```
