# Com - Documentació Retroshare: programari per compartir xifradament

Permet xat, compartir fitxers, ... tot de forma xifrada, amb varis nivells d'anonimicitat.
No destaca per la seva velocitat, però sí per ser un software distribuit.
Utilitza claus gpg que gestiona de forma molt simple per l'usuari, encara que permet ordres directes de consola.

Hem separat la documentació en tres parts:

- [connecta't](howto/retroshare-connecta.md) 
   - Aqui (howto/retroshare-connecta.md) recollim tot el que cal fer per instal.lar el software, connectar-se, tipus de connexions i manteniment general 
- [confia](howto/retroshare-confia.md) 
   - Aquesta part (howto/retroshare-confia.md) es sobre les relacions de confiança, com gestionarles... 
- [comparteix](howto/retroshare-comparteix.md) 
   - A (howto/retroshare-comparteix.md): bones pràctiques per compartir... 
