# install nextcloud

install apache

    apt-get install apache2 libapache2-mod-php7.0

install mysql

    apt-get install mariadb-server php7.0-mysql

install php

    apt-get install php7.0-gd php7.0-json php7.0-curl php7.0-mbstring

install php extensions

    apt-get install php7.0-intl php7.0-mcrypt php-imagick php7.0-xml php7.0-zip

install apache modules

    a2enmod php7.0 rewrite headers env dir mime

download latest nextcloud (today is 15.0.5)

    cd /var/www/html
    wget https://download.nextcloud.com/server/releases/nextcloud-15.0.5.zip
    unzip nextcloud-15.0.5.zip
    chown -R www-data: nextcloud

do apache configuration for nextcloud

    vi /etc/apache2/sites-available/nextcloud.conf

option /nextcloud - use this configuration if you want to access nextcloud through example.com/nextcloud - put in file `/etc/apache2/conf-available/nextcloud.conf`:

```
Alias /nextcloud "/var/www/nextcloud/"

<Directory /var/www/nextcloud/>
  Options +FollowSymlinks
  AllowOverride All

 <IfModule mod_dav.c>
  Dav off
 </IfModule>

 SetEnv HOME /var/www/nextcloud
 SetEnv HTTP_HOME /var/www/nextcloud

</Directory>
```

enable config

    a2enconf nextcloud.conf

option nextcloud.example.com - in case you use a subdomain like nextcloud.example.com - put in file `/etc/apache2/sites-available/nextcloud.example.com.conf`:

```
<IfModule mod_ssl.c>
  <VirtualHost _default_:443>
    DocumentRoot /var/www/html/nextcloud
    ServerName nextcloud.example.com

    <Directory /var/www/nextcloud/>
      Options +FollowSymlinks
      AllowOverride All

      <IfModule mod_dav.c>
       Dav off
      </IfModule>

      SetEnv HOME /var/www/nextcloud
      SetEnv HTTP_HOME /var/www/nextcloud
      Satisfy Any

    </Directory>

    <Directory "/var/www/html/nextcloud/data/">
      # just in case if .htaccess gets disabled
      Require all denied
    </Directory>

    <IfModule mod_headers.c>
      Header always set Strict-Transport-Security "max-age=15768000; preload"
    </IfModule>

    SSLEngine on
    SSLCertificateFile /etc/ssl/certs/ssl-cert-snakeoil.pem
    SSLCertificateKeyFile /etc/ssl/private/ssl-cert-snakeoil.key

    # add rewrites suggested by https://docs.nextcloud.com/server/15/admin_manual/issues/general_troubleshooting.html#service-discovery
    <IfModule mod_rewrite.c>
      RewriteEngine on
      RewriteRule ^/\.well-known/host-meta https://nextcloud.example.com/public.php?service=host-meta [QSA,L]
      RewriteRule ^/\.well-known/host-meta\.json https://nextcloud.example.com/public.php?service=host-meta-json [QSA,L]
      RewriteRule ^/\.well-known/webfinger https://nextcloud.example.com/public.php?service=webfinger [QSA,L]
      RewriteRule ^/\.well-known/carddav https://nextcloud.example.com/remote.php/dav/ [R=301,L]
      RewriteRule ^/\.well-known/caldav https://nextcloud.example.com/remote.php/dav/ [R=301,L]
    </IfModule>
  </VirtualHost>
</IfModule>
```

enable site for apache

    a2ensite nextcloud.example.com.conf

add in `/etc/php/7.0/apache2/php.ini`:

    [opcache]
    opcache.enable=1
    opcache.enable_cli=1
    opcache.interned_strings_buffer=8
    opcache.max_accelerated_files=10000
    opcache.memory_consumption=128
    opcache.save_comments=1
    opcache.revalidate_freq=1

replace `memory_limit` in `/etc/php/7.0/apache2/php.ini` to be:

    memory_limit = 512M

[optional] in case you want to have data in different disk/storage:

    mkdir -p /media/data/nextcloud
    chown -R www-data: /media/data/nextcloud
    cd /var/www/html/nextcloud
    mv data /media/data/nextcloud
    ln -s /media/data/nextcloud/data data

create user, database and choose privileges for nextcloud's case: run `mysql_setpermission` and select option 6

run in browser nextcloud and follow steps to install it

enter nextcloud, go to apps and install: calendar, polls, collabora online

# collabora online

install docker

```
sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg2 \
    software-properties-common

curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -

apt-key fingerprint 0EBFCD88

add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/debian \
   $(lsb_release -cs) \
   stable"

apt-get update

apt-get install docker-ce docker-ce-cli containerd.io

after that you have to reboot https://github.com/docker/for-linux/issues/598

source https://docs.docker.com/install/linux/docker-ce/debian/
```

`collaboraonline.example.com.conf` in case you use apache2 as reverse proxy (as seen in the official guide)

```
<VirtualHost *:443>
  ServerName collaboraonline.example.com:443

  # SSL configuration, you may want to take the easy route instead and use Lets Encrypt!
  SSLEngine on
  #SSLCertificateFile /etc/letsencrypt/live/collaboraonline.example.com/fullchain.pem
  SSLCertificateFile /etc/ssl/certs/ssl-cert-snakeoil.pem
  #SSLCertificateChainFile /path/to/intermediate_certificate
  #SSLCertificateKeyFile /etc/letsencrypt/live/collaboraonline.example.com/privkey.pem
  SSLCertificateKeyFile /etc/ssl/private/ssl-cert-snakeoil.key

  SSLProtocol             all -SSLv2 -SSLv3
  SSLCipherSuite ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA:ECDHE-RSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-RSA-AES256-SHA256:DHE-RSA-AES256-SHA:ECDHE-ECDSA-DES-CBC3-SHA:ECDHE-RSA-DES-CBC3-SHA:EDH-RSA-DES-CBC3-SHA:AES128-GCM-SHA256:AES256-GCM-SHA384:AES128-SHA256:AES256-SHA256:AES128-SHA:AES256-SHA:DES-CBC3-SHA:!DSS
  SSLHonorCipherOrder     on

  # Encoded slashes need to be allowed
  AllowEncodedSlashes NoDecode

  # Container uses a unique non-signed certificate
  SSLProxyEngine On
  SSLProxyVerify None
  SSLProxyCheckPeerCN Off
  SSLProxyCheckPeerName Off

  # keep the host
  ProxyPreserveHost On

  # static html, js, images, etc. served from loolwsd
  # loleaflet is the client part of LibreOffice Online
  ProxyPass           /loleaflet https://127.0.0.1:9980/loleaflet retry=0
  ProxyPassReverse    /loleaflet https://127.0.0.1:9980/loleaflet

  # WOPI discovery URL
  ProxyPass           /hosting/discovery https://127.0.0.1:9980/hosting/discovery retry=0
  ProxyPassReverse    /hosting/discovery https://127.0.0.1:9980/hosting/discovery

  # Main websocket
  ProxyPassMatch "/lool/(.*)/ws$" wss://127.0.0.1:9980/lool/$1/ws nocanon

  # Admin Console websocket
  ProxyPass   /lool/adminws wss://127.0.0.1:9980/lool/adminws

  # Download as, Fullscreen presentation and Image upload operations
  ProxyPass           /lool https://127.0.0.1:9980/lool
  ProxyPassReverse    /lool https://127.0.0.1:9980/lool

  # Endpoint with information about availability of various features
  ProxyPass           /hosting/capabilities https://127.0.0.1:9980/hosting/capabilities retry=0
  ProxyPassReverse    /hosting/capabilities https://127.0.0.1:9980/hosting/capabilities
</VirtualHost>
```

enable site

    a2ensite collaboraonline.example.com.conf

[nginx reverse proxy option](https://www.collaboraoffice.com/code/nginx-reverse-proxy/)

if your reverse proxy is in localhost (or change 127.0.0.1 to the interface you can securely listen)

    docker run -t -d -p 127.0.0.1:9980:9980 -e 'domain=nextcloud.example.com' -e 'dictionaries=en es ..' --restart always --cap-add MKNOD collabora/code

if you have firewall:

- ACCEPT traffic for docker0 bridge interface
- don't allow docker to modify firewall, put `DOCKER_OPTS="--iptables=false"` in `/etc/default/docker`

a way to validate that collabora online is working:

https://collaboraonline.example.com/hosting/capabilities

## issue related to docker and xfs malfunction

problem: xfs with d_type/ftype false is problematic with docker https://docs.docker.com/storage/storagedriver/overlayfs-driver/

workaround: create a new xfs partition

create new directory in that new partition for docker

    mkdir -p /media/data/docker

find docker.service

    systemctl status docker

go to `/lib/systemd/system/docker.service` and add the location (thanks https://www.rb-associates.co.uk/blog/move-var-lib-docker-to-another-directory/)

    ExecStart=/usr/bin/dockerd -g /media/data/docker -H fd:// --containerd=/run/containerd/containerd.sock

reload systemd config

    systemctl daemon-reload

force the usage of overlay2 (src https://docs.docker.com/storage/storagedriver/overlayfs-driver/). Add in `/etc/docker/daemon.json`

```json
{
  "storage-driver": "overlay2"
}
```

## install memcached

install APCu:

    apt install memcached php-apcu redis-server php-redis

[redis is significantly faster as a unix socket](https://guides.wp-bullet.com/how-to-configure-redis-to-use-unix-socket-speed-boost/), in `/etc/redis/redis.conf` put:

    unixsocket /var/run/redis/redis.sock
    unixsocketperm 775

add www-data to redis group and restart apache2 to have proper permissions

    gpasswd -a www-data redis
    service apache2 restart

configure APCU as memcache and redis for file locking, in `/var/www/html/nextcloud/config/config.php` add:

    'memcache.local' => '\OC\Memcache\APCu',
    'memcache.distributed' => '\OC\Memcache\Redis',
    'redis' => [
         'host'     => '/var/run/redis/redis.sock',
         'port'     => 0,
         'dbindex'  => 0,
         'timeout'  => 1.5,
    ],

https://docs.nextcloud.com/server/15/admin_manual/configuration_server/caching_configuration.html

## run occ operations

go to the appropriate directory

    cd /var/www/html/nextcloud

run your occ operation, for example:

    su www-data -s /bin/sh -c "php occ upgrade"
