When you have MTU problems, you cannot navigate to some places. This is a list to test sites

In openwrt you can test MTU problems remotely with `wget --no-check-certificate https://example.com`

# MTU not working

https://gitlab.com
https://www.ubnt.com
https://marca.com
https://easyjet.com
https://elmundo.es

# MTU working

https://google.com

# workaround for MTU

put a low MTU in the affected interface (considering for the example eth0)

    ip link set dev eth0 mtu 1300

adjust a low TCP MSS

create this two iptables configs

```
cat > iptables_mss <<EOF
*mangle
COMMIT
*filter
-A FORWARD -o eth0 -p tcp --tcp-flags SYN,RST SYN -j TCPMSS --set-mss 1000
COMMIT
*nat
COMMIT
EOF

cat > iptables_free <<EOF
*mangle
COMMIT
*filter
COMMIT
*nat
COMMIT
EOF
```

apply

    iptables-restore < iptables_mss

when you want to restore your firewall to defaults:

    iptables-restore < iptables_free

and

    ip link set dev eth0 mtu 1500
