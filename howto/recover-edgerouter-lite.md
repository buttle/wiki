# How to recover edgerouter lite

This recovers a edgerouter lite with a faulty internal usb (kernel panic) or if you want to change the USB so you have more space

Requirements:

- 4+ GB usb stick
- The console serial from rj45 to serial and serial to usb

Follow the steps here: https://github.com/sowbug/mkeosimg#usage


Edgerouter lite has an internal usb as a memory. Relevant images: https://hilo90mhz.com/ubiquiti-edgerouter-lite-erlite-3-internal-usb-storage-fail/

Hint: if the usb does not fit try quitting the case. ~~I used a toshiba 16 GB transmemory without case.~~ yes but it does not fit!

First connect the serial cable to edgerouter and your PC (or install it `sudo apt install picocom`):

    sudo picocom -b 115200 /dev/ttyUSB0

Power up edgerouter and start pressing enter until you see a prompt like `Octeon ubnt_e100#`. After that change bootloader of usb to perform a usb reset (thanks pedristic! -> https://community.ubnt.com/t5/EdgeRouter/List-of-Compatible-USB-drives/m-p/1866642/highlight/true#M154128):

    setenv bootcmd usb reset\;fatload usb 0 $loadaddr vmlinux.64\;bootoctlinux $loadaddr coremask=0x3 root=/dev/sda2 rootdelay=15 rw rootsqimg=squashfs.img rootsqwdir=w mtdparts=phys_mapped_flash:512k(boot0),512k(boot1),64k@3072k(eeprom)

Save new boot options

    saveenv

Restart device with a command

    reset

## backup environment

This was original `printenv`

```
bootcmd=fatload usb 0 $loadaddr vmlinux.64;bootoctlinux $loadaddr coremask=0x3 root=/dev/sda2 rootdelay=15 rw rootsqimg=squashfs.img rootsqwdir=w mtdparts=phys_mapped_flash:512k(boot0),512k(boot1),64k@3072k(eeprom)
bootdelay=0
baudrate=115200
download_baudrate=115200
nuke_env=protect off $(env_addr) +$(env_size);erase $(env_addr) +$(env_size)
autoload=n
ethact=octeth0
loadaddr=0x9f00000
numcores=2
stdin=serial
stdout=serial
stderr=serial
env_addr=0x1fbfe000
env_size=0x2000
flash_base_addr=0x1f800000
flash_size=0x400000
uboot_flash_addr=0x1f880000
uboot_flash_size=0x70000
flash_unused_addr=0x1f8f0000
flash_unused_size=0x310000
bootloader_flash_update=bootloaderupdate

Environment size: 675/8188 bytes
```

and final `printenv`

```
Octeon ubnt_e100# printenv
bootdelay=0
baudrate=115200
download_baudrate=115200
nuke_env=protect off $(env_addr) +$(env_size);erase $(env_addr) +$(env_size)
autoload=n
ethact=octeth0
loadaddr=0x9f00000
numcores=2
stdin=serial
stdout=serial
stderr=serial
env_addr=0x1fbfe000
env_size=0x2000
flash_base_addr=0x1f800000
flash_size=0x400000
uboot_flash_addr=0x1f880000
uboot_flash_size=0x70000
flash_unused_addr=0x1f8f0000
flash_unused_size=0x310000
bootloader_flash_update=bootloaderupdate
bootcmd=usb reset;fatload usb 0 $loadaddr vmlinux.64;bootoctlinux $loadaddr coremask=0x3 root=/dev/sda2 rootdelay=15 rw rootsqimg=squashfs.img rootsqwdir=w mtdparts=phys_mapped_flash:512k(boot0),512k(boot1),64k@3072k(eeprom)

Environment size: 685/8188 bytes
```
