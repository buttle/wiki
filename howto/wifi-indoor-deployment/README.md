Two kinds of deployments for indoor case

- [quickstart](quickstart.md): a very easy way to do a deployment that is inexpensive (you can reuse the routers you want)
- [corporate](corporate.md): a complete wifi indoor deployment intended to serve an organization giving access to all clients and facilitating the person responsible of it its management
