# corporate wifi

We selected Unifi AC Lite because it has a good relation between price and quality. It also allows you to put openwrt inside.

## management / troubleshooting

- Method to flash these devices https://openwrt.org/toh/ubiquiti/unifiac#non-invasive_method_using_mtd
- vlan support is nice https://forum.openwrt.org/t/unfi-ap-ac-lr-vlan-support/7808/4
- UniFi - LED Color Patterns in UniFi Devices: https://help.ubnt.com/hc/en-us/articles/204910134
- UniFi - TFTP Recovery for Bricked Access Points: https://help.ubnt.com/hc/en-us/articles/204910124-UniFi-TFTP-soft-recovery-for-bricked-access-point

## TODO

- testing a wifi open source controller is pending: https://github.com/aparcar/meshrc

## extra articles

- https://openwrt.org/docs/guide-user/network/wifi/guestwifi/configuration
- https://openwrt.org/docs/guide-user/network/wifi/guestwifi/guest-wlan
- https://openwrt.org/docs/guide-user/network/wifi/guestwifi/guest-wlan-webinterface
