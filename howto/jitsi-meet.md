<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**

- [Quick install instructions](#quick-install-instructions)
- [Disable jicofo health check](#disable-jicofo-health-check)
  - [Why](#why)
- [Disable third party stuff](#disable-third-party-stuff)
- [Enable Screen Capture for Firefox](#enable-screen-capture-for-firefox)
- [Enable Screen Capture for Chromium/Chrome](#enable-screen-capture-for-chromiumchrome)
- [Troubleshooting](#troubleshooting)
- [More info](#more-info)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

Deploy your own meet.jit.si service

First of all I recommend that before starting this guide you have a public IP directly attached to the server jitsi is going to be installed. Jitsi allows NAT, etc. but I could not make it work without that public IP. If you can, please share it and I will put it here.

# Quick install instructions

https://github.com/jitsi/jitsi-meet/blob/master/doc/quick-install.md

# Disable jicofo health check

/etc/jitsi/jicofo/sip-communicator.properties

    org.jitsi.jicofo.HEALTH_CHECK_INTERVAL=-1

and restart jicofo

    service jicofo restart

## Why

Read if you are interested on knowing why this option is recommended

[As said by a jitsi developer](https://github.com/jitsi/jitsi-meet/issues/2172#issuecomment-389589242) health checks are for environments with multiple jvbs instances (this is the scenario of meet.jit.si, with multiple instances around the world attending the same service); in other words, there is no sense to have the health check for a single jvb instance. [The problem](https://github.com/jitsi/jitsi-meet/issues/2172) (at least with a single jvb instance) is that the healthcheck opens supposedly too much UDP ports that are not immediately closed after the healtcheck, hence, jicofo crashes and "jitsi is not working"

# Disable third party stuff

Third party stuff example: the random funny avatars

/etc/jitsi/meet/meet.tips.es-config.js

    disableThirdPartyRequests: true,

# Enable Screen Capture for Firefox

```diff
- desktopSharingFirefoxDisabled: true,
+ desktopSharingFirefoxDisabled: false,
// (...)
- desktopSharingFirefoxMaxVersionExtRequired: -1,
+ desktopSharingFirefoxMaxVersionExtRequired: 51,
```

src https://github.com/jitsi/jidesha/blob/master/firefox/README.md#deprecation

# Enable Screen Capture for Chromium/Chrome

[Since chrome/chromium 72 extension is not needed anymore.](https://github.com/jitsi/jidesha/issues/47). So it should work out of the box :)

# Troubleshooting

Important logs to inspect

- /var/log/jitsi/jicofo.log - jitsi meet (javascript, and binding to videobridge and prosody)
- /var/log/jitsi/jvb.log - jitsi videobridge
- /var/log/prosody/prosody.log - XMPP server (chat, room allocation)

try restarting services (or reboot):

    service jicofo restart
    service jitsi-videobridge restart
    service prosody restart

# More info

- Jitsi Videobridge documentation - https://github.com/jitsi/jitsi-videobridge/blob/master/doc
