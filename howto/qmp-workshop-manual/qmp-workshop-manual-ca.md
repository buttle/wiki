---
author: 'guifi.net community'
title: 'taller de quick Mesh project (qMp.cat)'
---

Introducció
===========

Material bàsic per desplegar xarxes qMp

Els dispositius acostumen a utilitzar Power over Ethernet (PoE) de 24 V,
això vol dir que la potència elèctrica i les dades del dispositiu van a
través del cable ethernet. El cable blau en la figura <fig:poe> es el
que s'està desplegant cap a un lloc exterior (normalment una terrassa o
un teulat) on els dispositius qMp seran instalats i prendran
l'interfície PoE de l'injector PoE. L'altre interfície en l'injector PoE
és la LAN [^1] i pot emplaçar-se cap a un PC o un switch/router si es
vol connectar més d'un PC. Finalment, l'injector PoE requereix
alimentació elèctrica estàndard. **Alerta**: una connexió PoE cap a un
dispositiu que no estigui preparat per treballar en PoE 24 V, per
exemple una interfície de xarxa d'un ordinador, pot causar averia de la
interfície de xarxa.

![Diagrama de xarxa amb PoE](./img/general/poe.jpg "poe")

Elements bàsics d'instal·lador: Components
==========================================

-   Cable d'exteriors i connectors RJ45 (es venen per separat). El cable
    hauria de ser d'exteriors per resistir el clima.
    -   Cable UTP: cable sense protecció. Però a vegades és suficient
        pels nostres propòsits.
    -   Cable FTP (recomanat): cable amb protecció bàsica. Preveu
        descàrrega electrostàtica [^2]. La diferència és que el cable no
        té terra.
-   Eina de crimpatge: per unir el connector RJ45 amb el cable
-   Comprovador (tester) de cable (recomanat): un de baix cost ens
    assegura un correcte crimpat del cable.
-   Tallador de cable: Per agafar un segment de cable per
    la instal·lació. Nota: El cable UTP es més fàcil de tallar que el
    FTP
-   Brida: Per fixar el cable i el dispositiu al màstil o amb
    altres cables.

Elements bàsics d'instal·lador: Dispositius 5 GHz
=================================================

Els dispositius seleccionats treballen a la banda de 5 GHz perquè 2.4
GHz té un ús molt estès en la població i té moltes interferències. Són
del fabricant Ubiquiti i són compatibles amb el firmware qMp.

Baix cost

:   al voltant dels 70 euro.

    Nanostation Loco M5 (NSLM5)
    :   Petites distàncies (menys d'1 km). La connexió amb un altre node
        candidat és acceptable i no cal incrementar la potència de
        senyal de wifi. Utilitza el mateix firmware que
        la NanostationM5.

    Nanostation M5 (NSM5)

    :   

    NanoBeam M5 (NBM5) diferents models

    :   

    sèrie XW
    :   Són les noves versions dels dispositius NSLM5, NSM5, etc. però
        amb processadors nous. El primer introduït va ser NBM5
        reemplaçant a NanoBridge M5 (en procés de descatalogar-se).
        Aquest dispositiu requereix qMp 4 i no és estable encara. La
        resta de dispositius especificats són AirMax (XM) i s'utilitzen
        a qMp 3.1 (versió estable).

Cost alt

:   entre 100 i 300 euro.

    Rocket M5

    :   

Resum d'informació rellevant a la Taula <tab:devspec>:

  ----------------- ---------------- --------------------- ------- ------ -------
  Devices           Gain (dBi)       Beamwidth (deg)       Proc.   RAM    Flash
                                     Hpol/Vpol/Elevation           (MB)   (MB)
  NSLM5             13               45/45/45              24KC    32 S   8
  NSM5              16               43/41/15              24KC    32 S   8
  NBM5              16, 19, 22, 25   veure en PDF          74KC    64 D   8
  XW series         -                -                     74KC    -      -
  RM5 S90 MG, HG    17, 20           veure en PDF          24KC    64 S   8
  RM5 S120 MG, HG   16, 19           veure en PDF          24KC    64 S   8
  RM5 D             30, 34           veure en PDF          24KC    64 S   8
  ----------------- ---------------- --------------------- ------- ------ -------

  : Especificacions dels dispositius

Proc

:   Especificacions de processador.

    24KC
    :   Atheros MIPS 24KC, 400MHz

    74KC
    :   Atheros MIPS 74KC, 560MHz

RAM

:   Tipus de RAM:

    S
    :   SDRAM

    D
    :   DDR2

Operacions bàsiques a qMp: Operacions de testeig
================================================

La figura <fig:wan-status-on> mostra la primera pantalla obtinguda quan
es fa log in a un node qMp

![Primera
pantalla](./img/qMp-basics-scrot/status-wan_status.png "wan-status-on")

\noindent

Per tornar a aquesta pantalla, aneu al menu i cliqueu a:

``` {.example}
qMp/Mesh / Status
```

alternativament:\
<http://admin.qmp/cgi-bin/luci/qmp/status>

Al baixar per la pantalla, apareixen les estacions associades
(Associated Stations). La figura <fig:associated-stations> té els
enllaços de wifi amb altres nodes qMp i amb quina senyal s'associen (en
dBm). Les bones pràctiques de guifi.net diuen que un enllaç troncal ha
de ser millor que -75dBm [^3]. En aquesta figura hi ha diferents tipus
de qualitat d'enllaç. Bona qualitat significa alts paràmetres de: dBm,
RX Rate, TX Rate \[ample de banda (Mbps)\] i codificació MCS (el
número).

Aquestes qualitats es refereixen a la connexió amb diferents nodes,
només mostra la direcció MAC. Però amb la MAC es suficient per
identificar el node, ja que els darrers quatre caràcters de la MAC es
posen al final del nom del node. Més endavant, es mostrarà com anar a
altres nodes de la xarxa.

![Estations
associades](./img/qMp-basics-scrot/status-associated-nodes.png "associated-stations")

Un altre mesura de qualitat es mostrada a la Figura <fig:links-node>.
Aquesta és la qualitat en termes del protocol d'enrutament de la xarxa
(bmx6) amb una escala de 0-100 per la recepció i la transmissió (rx/tx).

\noindent

Per arribar a allà, aneu al menú clicant a:

``` {.example}
qMp/Mesh / Mesh / Links
```

alternativament:\
<http://admin.qmp/cgi-bin/luci/qmp/mesh/links>

![Enllaços del node](./img/qMp-basics-scrot/links.png "links-node")

També, es pot fer el test de rendiment en ample de banda entre els
nodes. La figura <fig:bw-test> executa una prova del rendiment de la
connexió i dona els Mbps entre el node des d'on som i altres possibles
destinacions. Aplica els test d'un en un per saber l'ample de banda d

\noindent

Per arribar a allà, aneu al menú clicant a:

``` {.example}
qMp/Mesh / Tools
```

alternativament:\
<http://admin.qmp/cgi-bin/luci/qmp/tools>

![Test de rendiment en ample de
banda](./img/qMp-basics-scrot/test-bandwidth.png "bw-test")

Figura <fig:wifi-signal-rt>. Després de l'scan general, quan hi ha un
node candidat per fer una connexió estable, hi ha la necessitat
d'analitzar la qualitat de l'enllaç en temps real. Això ajuda a
seleccionar un lloc òptim per fixar el dispositiu en la instal·lació.

\noindent

Per arribar a allà, aneu al menú clicant a:

``` {.example}
OpenWRT / Status / Realtime Graphs / Wireless
```

alternativament:\
<http://admin.qmp/cgi-bin/luci/admin/status/realtime/wireless>

![Guany del millor senyal wifi rebut en temps
real](./img/qMp-basics-scrot/realtime_wifi_link.png "wifi-signal-rt")

La situació pot ser que no pot haver connexió del node a la xarxa.
Potser la xarxa està a un altre canal. La figura <fig:find-qmp> mostra
un scan del wifi. qMp sempre utilitza el BSSID: `02:CA:FF:EE:BA:BE`, en
el Mode `Ad-Hoc`. Aquestes són dos sòlides referències per trobar altres
xarxes qMp. En la figura hi han dos xarxes qMp en canals: 140 i 132.

\noindent

Per arribar a allà, aneu al menú clicant a:

``` {.example}
OpenWRT / Network / Wifi / "Scan"
```

alternativament:\
<http://admin.qmp/cgi-bin/luci/admin/network/wireless> and click Scan.

![Scan de Wifi: trobar xarxa
qMp](./img/qMp-basics-scrot/wifi_scan_find_qmp.png "find-qmp")

Si s'està dissenyant una nova xarxa qMp és important escollir un canal
que no estigui en ús. La figura <fig:interference> mostra com un altre
Punt d'accés (Acces Point, AP) està utilitzant el canal 140.

![Scand de wifi amb
interferències](./img/qMp-basics-scrot/wifi_scan_interference.png "interference")

La figura *wifi-channel-power* mostra on canviar els paràmetres del
wifi, per exemple, canal i la potència del senyal wifi. Per defecte, qMp
utilitza potència de senyal 17, però pot ser incrementada.

Utilitza la potència del senyal wifi amb compte, en la xarxa d'interès
és una senyal de comunicació i/o cobertura, però per altres xarxes és un
altre soroll en l'entorn que fa les comunicacions més difícils.

\noindent

Per arribar a allà, aneu al menú clicant a:

``` {.example}
OpenWRT / Node configuration / Wireless Settings
```

alternativament:\
<http://admin.qmp/cgi-bin/luci/qmp/configuration/wifi/>

![Wifi: Canal i potència de
senyal](./img/qMp-basics-scrot/wifi-channel-power.png "wifi-channel-power")

La figura <fig:tunnels>, marcat com vermell, mostra que hi ha un node
WAN, el node fa un anunci de la seva xarxa com `Internet`. Si pot
arribar allà, vol dir que hi ha connexió a internet, prova des d'un
navegador web. També pot ser interessant executar diferents test de
velocitat d'internet [^4] [^5] [^6] [^7].

Però potser no es pot arribar al node WAN, o no hi ha node WAN a la
xarxa. Es pot comprovar si hi ha un túnel a Internet.

En la mateixa vista, es pot buscar per node Frontera. La figura
<fig:tunnels> mostra, marcat en blau, com el node fa un anunci de xarxa
en `10.0.0.0/8`, això vol dir, accés a la resta de guifi.net

\noindent

Per arribar a allà, aneu al menú clicant a:

``` {.example}
qMp/Mesh / Mesh / Tunnels
```

alternativament:\
<http://admin.qmp/cgi-bin/luci/qmp/Mesh/Tunnels>

![Túnels](./img/qMp-basics-scrot/tunnels.png "tunnels")

Operacions bàsiques a qMp: Instal·lació bàsica i manteniment
============================================================

Figura <fig:quick-setup>, aquesta és la configuració final del node quan
està preparat per estar en la fase "en proves", és a dir, s'espera un
funcionament correcte però s'ha connectat recentment.

En la pàgina web de guifi.net, després d'afegir els dispositius, es rep
una IPv4 única dins de guifi.net, i és necessari una màscara del tipus
`255.255.255.244`. Utilitza el mateix nom de node que en la pàgina web o
la web d'organització de la xarxa.

\noindent

Per arribar a allà, aneu al menú clicant a:

``` {.example}
qMp/Mesh / Node configuration / qMp easy setup
```

alternativament:\
<http://admin.qmp/cgi-bin/luci/qmp/configuration/easy_setup/>

![Configuració ràpida del
dispositiu](./img/qMp-basics-scrot/quick_setup.png "quick-setup")

Figura <fig:backup>: Quan el node està funcionant bé és important fer
una còpia de seguretat de la configuració. Amb qMp, no està recomanat
actualitzar el node utilitzant aquest menu.

\noindent

Per arribar a allà, aneu al menú clicant a:

``` {.example}
OpenWRT / System / "Backup/Flash Firmware"
```

alternativament:\
<http://admin.qmp/cgi-bin/luci/admin/system/flashops>

![Còpia de
seguretat](./img/qMp-basics-scrot/backup-new-firmware.png "backup")

Per actualitzar el node de moment només és possible a través de la línia
de comandes (Command Line Interface, CLI) . Fes un login en la sessió
ssh:

``` {.example}
ssh root@admin.qmp
```

password: 13f\
A partir d'aquest punt hi ha tres mètodes:

1.  Actualització automàtica (amb connexió a internet des del node).

    ``` {.example}
    qmpcontrol upgrade
    ```

2.  Actualització amb un enllaç (amb connexió a internet des del node).

    ``` {.example}
    qmpcontrol upgrade "http://...qmp.bin"
    ```

    És a dir la URL on està localitzat el binari de qMp, recorda que es
    poden trobar tots els binaris suportats en: <http://fw.qmp.cat>
3.  Actualitzar utilitzant un fitxer local (sense connexió a internet
    des del node)
    1.  Posa el fitxer dins del node qMp, obre una línea de comandes i
        posa

        ``` {.example}
        scp qmp.bin root@admin.qmp:/tmp
        ```

        Et preguntarà per la contrassenya
    2.  Amb la connexió ssh existent oberta anteriorment, o amb una
        nova, login per ssh i:

        ``` {.example}
        qmpcontrol upgrade "/tmp/qmp.bin"
        ```

Confirma i continua amb el procés d'actualització i espera fins que
finalitzi.

Nota: qMp només guarda configuracions comunes després de
l'actualització, concretament:

``` {.example}
# cat /etc/config/qmp | grep preserve
```

Per altres fitxers canviats, fes un backup abans d'actualitzar

Operacions bàsiques a qMp: Visualitza la xarxa
==============================================

La figura <fig:net-nodes> mostra una pantalla on són tots els nodes qMp
que conformen la xarxa. Fent click a l'icona esfera blava a la dreta de
cada node és possible

\noindent

Per arribar a allà, aneu al menú clicant a:

``` {.example}
qMp/Mesh / Mesh / Nodes
```

alternativament:\
<http://admin.qmp/cgi-bin/luci/qmp/mesh/nodes>

![Adreces IP dels
nodes](./img/qMp-basics-scrot/net-of-nodes.png "net-nodes")

La figura <fig:graph-network> és un graf que mostra els nodes, les
arestes amb la qualitat de bmx6 mostra com estan connectats entre ells.

\noindent

Per arribar a allà, aneu al menú clicant a:

``` {.example}
qMp/Mesh / Mesh / Graph
```

alternativament:\
<http://admin.qmp/cgi-bin/luci/qmp/mesh/graph>

![Graf de la xarxa](./img/qMp-basics-scrot/graph.png "graph-network")

Propostes de disseny d'una xarxa qMp: disseny del node WAN
==========================================================

Per construir el node WAN, la figura <fig:wan-gen> mostra com un node
qMp s'hauria de connectar a la xarxa (a través del wifi amb el protocol
d'enrutament bmx6) i d'Internet (a través de l'ethernet cap al router de
l'ISO [^8] amb el DHCP client).

Es recomana utilitzar el dispositiu Nanostation M5 ja que té dos
interfícies de xarxa (eth0, eth1). Amb una es pot fer un DHCP server per
connectar a un node qMp des d'un portàtil. I amb l'altre ethernet, un
DHCP client per connectar al router de l'ISP.

Si és una Nanostation Loco M5, només té una ethernet (eth0 [^9]).
Aquesta interfície serà utilitzada per la connexió a l'accés a internet
a través del ISP router (DHCP client). Això vol dir que no hi ha forma
automàtica de connectar-se al node qMp des del portàtil (no hi ha DHCP
server). Per accedir a aquest node, utilitza un altre node qMp que
estigui en la mateixa xarxa (utilitzant la interfície wifi).

![Diagrama de xarxa genèric amb el WAN
node](./img/mesh-designs/wan_node_generic.png "wan-gen")

Per establir la ethernet que farà de DHCP client cap al router ISP hi ha
2 opcions.

Opció 1: en el quick setup, l'última part parla de què fer amb les
interfícies (figura <fig:quickdhcp>). Les interfícies tenen 3
seleccions: `Mesh`, `Lan` (DHCP server) i `WAN` (DHCP client).

![Opció 1: Establir el DHCP client a una interfície amb el quick
setup](./img/qMp-basics-scrot/quick_setup_interfaces.png "quickdhcp")

Opció 2: La figura <fig:netset> mostra la pantalla per establir la
interfície de DHCP client, i no hi ha necessitat de fer un quick setup
en el node.

\noindent

Per arribar a allà, aneu al menú clicant a:

``` {.example}
OpenWRT / Node configuration / Network Settings
```

alternativament:\
<http://admin.qmp/cgi-bin/luci/qmp/configuration/network/>

![Opció 2: Establir a la interfície el DHCP client amb el menu network
settings](./img/qMp-basics-scrot/network_settings.png "netset")

Per provar que està funcionant el DHCP client cap al router ISP,
comprova el IPv4 WAN Status, secció Xarxa. La figura
<fig:wan-status-on-detail> mostra una exitosa connexió WAN. La figura
<fig:wan-status-off> mostra que no hi ha connexió WAN: no hi ha DHCP
client o no està correctament connectat.

![WAN status
online](./img/qMp-basics-scrot/status-wan_status_detail.png "wan-status-on-detail")

![WAN status
offline](./img/qMp-basics-scrot/wan_not_connected.png "wan-status-off")

\noindent

Per arribar a allà, aneu al menú clicant a:

``` {.example}
qMp/Mesh / Mesh / Status
```

alternativament:\
<http://admin.qmp/cgi-bin/luci/qmp/status>

Propostes de disseny d'una xarxa qMp: disseny general d'un node
===============================================================

La figura <fig:gen-node> mostra els elements de la instal·lació d'un
node senzill: un node qMp connectat a la xarxa i un router wifi en 2.4
GHz com AP per donar cobertura dins d'un espai.

![Diagrama de xarxa d'un node
genèric](./img/mesh-designs/generic_node.png "gen-node")

Flash a un node qMp
===================

Passos per *flashejar* o posar el sistema qMp en el dispositiu.
S'assumeix que es fa des d'un ordinador Ubuntu/Debian GNU/Linux i una
antena de 5 GHz recomanada prèviament:

1.  Descarrega la imatge **Factory** [^10] per un dispositiu suportat
    que té el sistema del fabricant [^11]. La imatge **Sysupgrade** és
    per nodes on hi ha OpenWRT o qMp i que es volen actualitzar. La
    imatge **Guifi** té millor integració amb la web de guifi.net.
2.  Reanomena el fitxer descarregat per `qmp.bin`
3.  Descarrega els packets tftp des del teu repositori del sistema. Des
    de la CLI: `$sudo apt-get install tftp-hpa`.
4.  Desconnecta la connexió a internet.
5.  Obre una CLI i escriu:

    ``` {.example}
    $ ping 192.168.1.20
    ```

    T'ajudarà saber quan el dispositiu està en el mode reset.
6.  Connecta els equipaments com es mostra a la Figura
    <fig:flashdiagram>.

    ![Diagrama de xarxa per flashejar un
    dispositiu](./img/general/flashdiagram.jpg "flashdiagram")

7.  Configura la xarxa amb una de les següents opcions:
    1.  **Opció GUI**: configura amb el teu administrador de xarxa
        preferit una ethernet amb IP estàtica en l'ordinador per
        connectar-lo al dispositiu:\
        IP: 192.168.1.10\
        Mascara de xarxa (Subnet): 192.168.1.100\
        Porta d'enllaç (Gateway): 192.168.1.1
    2.  **Opció CLI**:

        ``` {.example}
        $ sudo ip a a 192.168.1.25/24 dev eth0
        ```

8.  Posa el dispositiu a mode TFTP server amb una d'aquestes opcions:
    1.  **Reset des del dispositiu**: Desconnecta la interfície
        del dispositiu. Treu la tapa del dispositiu. Amb una mà agafa un
        objecte amb punta rodona i apreta de forma continuada el botó de
        reset (Figura <fig:resetant>). Mentrestant amb l'altre mà
        inserta el cable ethernet a l'interfície del dispositiu

        ![Reset device](./img/general/reset-device.jpg "resetant")

    2.  **Reset des de l'injector PoE**: Comprova que el dispositiu va
        amb reset al PoE (Figura <fig:resetpoe>). Desconnecta la
        interfície PoE de l'injector PoE. Amb una mà agafa un objecte
        amb punta rodona i apreta de forma continuada el botó de reset.
        Mentrestant amb l'altre mà inserta el cable ethernet a
        l'interfície PoE de l'injector PoE.

        ![Botó reset a l'injector
        PoE](./img/general/reset-injector.jpg "resetpoe")

9.  Observa si el dispositiu comença el mode reset per continuar:
    -   **Opció leds en el dispositiu**: Espera mentre el led 1 i 3
        canvien fins a 2 i 4 cíclicament. Amb aquest recurs de vídeo et
        faràs una idea del temps i els colors dels leds involucrats al
        procés [^12].
    -   **Opció pantalla de l'ordinador**: el ping començarà
        a respondre. La sortida de `ping 192.168.1.20` hauria de ser
        similar a:

        ``` {.example}
        64 bytes from X: icmp_req=X ttl=X time=X ms
        ```

10. Si està en mode reset deixa d'apretar el botó de reset i posa el
    dispositiu en una zona estable.
11. Obre una finestra de CLI, ves on està descarregat el firmware
    (sistema qMp) `qmp.bin`.

    ``` {.example}
    cd /lloc/on/es/el/directori_qmpbin
    ```

    I després, executa:

    ``` {.example}
    $ tftp 192.168.1.20
    $ mode octet
    $ trace
    $ put qmp.bin
    ```

    \[ Procés de transmissió \]

    ``` {.example}
    $ quit
    ```

12. Després de 5 minuts, el 4rt led de la rampa (el de més a la dreta,
    figura <fig:ledsdevice>) està encès, no parpadejant. Aquest és el
    moment per anar al proper pas.

    ![Sistema Led del
    dispositiu](./img/general/blinkingled.jpeg "ledsdevice")

13. Reconfigura la xarxa per fer un DHCP client en un port ethernet
    (IP Automàtica) i prova de connectar de nou l'ordinador
    al dispositiu.
14. Comprova que el dispositiu respon al ping:

    ``` {.example}
    $ ping 172.30.22.1
    ```

    Aquesta és l'adreça fixa en el mode roaming (per defecte a qMp).\
    Una forma més general és aconseguir la porta d'enllaç (gateway):

    ``` {.example}
    $ ip r | grep default | cut -f3 -d' '
    ```

    Obre el navegador web i comprova si aquesta web es pot accedir
    (**Alerta** només funcionarà si el PC s'ha connectat al dispositiu
    amb DHCP):

    ``` {.example}
    http://admin.qmp
    ```

    alternatives:

    ``` {.example}
    http://172.30.22.1
    http://<gateway_ip>
    ```

15. El login d'accés és usuari: root\
    contrassenya: 13f

Altres referències [^13] [^14] [^15]

Fent una panoràmica amb Hugin
=============================

Amb el programa Hugin és molt fàcil fer fotos panoràmiques, i és
software lliure [^16]. Les panoràmiques són molt útils per saber
visualment quins són els nodes propers.

1.  Com fer les fotos? Manté la mateixa ubicació física i comença a fer
    fotos amb un solapament del 20% entre elles.
2.  Segueix els passos al programa Hugin (figura: <fig:hugin>)
    1.  `1.Load images`, selecciona totes les imatges el directori amb
        el qual es vol fer la panoràmica.
    2.  `2.Align`.
        -   això processa els punts en comú (punts de control) en les
            diferents fotos per donar sensació de continuïtat.
        -   si no hi ha suficients punts de control, busca els punts de
            control manualment o torna a fer les fotografies.

    3.  `3.Create panorama`: guarda la foto en fitxers .pto i .tiff en
        el directori on són totes les imatges.

    ![Hugin](./img/general/hugin.png "hugin")

3.  Conversió de .tiff a .jpeg\
    Si es vol compartir la panoràmica.

    ``` {.example}
    sudo apt-get install imagemagick
    convert pan.tiff pan.jpeg
    ```

    Un exemple es mostra a la figura <fig:exhugin>

    ![Exemple de panoràmica utilitzant
    hugin](./img/santandreudeploy/llenguadoc.jpg "exhugin")

Sobre monitorització de la xarxa
================================

Monitoritzar la xarxa és important com a mesura de qualitat. Es
presenten 3 alternatives.

**Des de la web de guifi.net**
------------------------------

es poden obtenir els gràfics. Ajuda a saber si els dispositius està
funcionant, la seva latència (ping) i el tràfic de xarxa. La figura
<fig:snpservices> mostra com es veu.

![Servidor de gràfiques a
guifi.net](./img/general/snpservices.png "snpservices")

Es requereix una versió qMp amb el paquet guifi: hauria d'aparèixer
`qMp-Guifi` al fitxer descarregat.

La part del server utilitza el paquet desenvolupat per la comunitat de
guifi.net anomenat `snpservices`. Per instal·lar-lo es pot seguir
aquesta guia [^17], bàsicament, s'obté un repositori de Debian,
s'instal·la el paquet i es dona l'identificador (id) del servidor de
gràfiques (altres paràmetres per defecte). Per obtenir l'id del servidor
de gràfiques s'ha de crear un servei de gràfiques a la web de guifi.net.
Per exemple, el id del servidor de gràfiques de Barcelona es pot tenir
de la URL: `http://guifi.net/en/node/55045`, i és `55045`.

qMp utilitza el paquet `mini_snmpd` [^18] configurat de la web de
guifi.net. Després de crear el node i el trasto a la web, es genera el
fitxer `unsolclic`. La figura <fig:qmpquifi> mostra com de simple és:
posar allà la URL del dispositiu i aplicar.

![menú guifi.net al sistema
qMp](./img/qMp-basics-scrot/qmpguifi.png "qmpguifi")

**munin**:
----------

Per GNU/Linux Debian 7 Wheezy server (apache 2.2)

``` {.example}
sudo apt-get install munin
```

per defecte fa la monitorització del propi servidor on està funcionant
(localhost).

Per posar els gràfics disponibles a tots els usuaris de la comunitat
guifi.net [^19], seguint el model de tenir accés a les dades de la
xarxa, canvia les següent línies a `/etc/munin/apache.conf`:

``` {.example}
Order allow,deny
Allow from localhost 127.0.0.0/8 ::1
Options None
```

de la següent manera:

``` {.example}
Order allow,deny
Allow from all
Options FollowSymLinks SymLinksIfOwnerMatch
```

Apply the changes in the HTTP server:

``` {.example}
# service apache2 restart
```

Afegeix nodes qMp per monitoritzar editant el fitxer
`/etc/munin/munin.conf`:

``` {.conf}
[qMp-node1] address 10.x.x.x
use_node_name yes
[qMp-node2] address 10.x.x.x
use_node_name yes
```

Aplica els canvis en el monitor (començaran a aparèixer després de pocs
minuts):

``` {.example}
# service munin-node restart
```

Els gràfics són similars als de la web de guifi, però donen més
informació. Excepte que hi ha un error que no permet veure el tràfic de
xarxa.

**qmpsu**:
----------

De moment, no hi ha un paquet genèric de qmpsu per les xarxes qMp, només
al barri de Sants (Barcelona). La figura <fig:qmpsu> mostra com és.

![Mostra de qmpsu](./img/general/qmpsu.png "qmpsu")

Footnotes
=========

[^1]: Per tenir una referència, el cable LAN pot fer de llarg 100 m
    només transportant dades

[^2]: <http://en.wikipedia.org/wiki/Electrostatic_discharge>

[^3]: <http://guifi.net/ca/BonesPractiquesUER>

[^4]: <http://www.catnix.net/en/speedtest>

[^5]: <http://speedtest.net>

[^6]: <http://testdevelocidad.es>

[^7]: <http://testvelocidad.eu/>

[^8]: Internet Service Provider

[^9]: eth1 is ignored

[^10]: <http://fw.qmp.cat/>

[^11]: <http://qmp.cat/Supported_devices>

[^12]: [https://www.youtube.com/watch?v=xIflE\_-V-B4\\\#t=50s](https://www.youtube.com/watch?v=xIflE_-V-B4\#t=50s)

[^13]: <http://wiki.ubnt.com/Firmware_Recovery>

[^14]: [http://www.qmp.cat/\\\#Use-the-firmware](http://www.qmp.cat/\#Use-the-firmware)

[^15]: tftp info:
    <http://wiki.openwrt.org/doc/howto/generic.flashing.tftp>

[^16]: <http://hugin.sourceforge.net/>

[^17]: <http://ca.wiki.guifi.net/wiki/Servidor_de_gr%C3%A0fiques_1>

[^18]: <http://wiki.openwrt.org/doc/howto/snmp.server>

[^19]: Solució per apache 2.2 i 2.4:
    <http://stackoverflow.com/questions/9127802>
