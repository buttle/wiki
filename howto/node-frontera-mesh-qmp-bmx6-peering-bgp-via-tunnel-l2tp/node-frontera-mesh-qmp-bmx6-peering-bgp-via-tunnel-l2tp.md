# Configuració d'un router amb OpenWrt com a frontera amb Guifi.net per a una xarxa mesh basada en qMp i BMX6 amb peering BGP a través d'un túnel L2TP

## Introducció
Aquest document descriu el procés per configurar un router amb OpenWrt per tal que operi com a **node frontera** a Guifi.net per a una xarxa mesh que funciona amb BMX6 i qMp (i.e., entre la pròpia mesh i la resta de Guifi).
El dispositiu exporta les rutes de la mesh i importa les rutes de Guifi.net mitjançant un *peering* contra un altre router de Guifi.net a través d'un túnel L2TP.

## Dades bàsiques
### Dispositius
Fem servir un router [Ubiquiti EdgeRouter™ SFP](https://www.ubnt.com/edgemax/edgerouter-x-sfp/ "Ubiquiti EdgeRouter™ X-SFP"), que compta amb 5 ports Gigabit Ethernet + PoE out i permet alimentar des de l'interior la resta de dispositius del node, que es troben a l'exterior, al qual li instal·lem OpenWrt.

Els dispositius exteriors són dos routers qualssevol que facin mesh amb qMp ([Ubiquiti NanoStation® M5](https://www.ubnt.com/airmax/nanostationm/ "Ubiquiti NanoStation® M5"), etc.) per WiFi i per cable.

### Paràmetres de xarxa, etc. <a name="sss-dades-xarxa"></a>
El [node](https://guifi.net/ca/node/107286 "UPC-CN-C5-Terrat") en qüestió té assignat el sub-rang d'adreces 10.1.24.0/27, dins del rang de la zona [mesh del Campus Nord de la UPC](https://guifi.net/ca/node/46433 "Mesh del Campus Nord") i de la Zona Universitària (10.1.24.0/21).

Al dispositiu que fa de frontera farem servir la primera adreça del rang (10.1.24.1/27) i a la resta de dispositius les següents de forma consecutiva (10.1.24.2/27, 10.1.24.3/27, etc.).

El node disposa d'una connexió a Internet a través de la xarxa de la UPC i una adreça IPv4 pública assignada per DHCP (147.83.x₁.y₁). Farem servir aquesta connexió només per aixecar un túnel L2TP contra un altre router amb adreçament públic de la UPC (147.83.x₂.y₂) i intercanviar rutes i trànsit, no pas per oferir una sortida directa des de la mesh cap a Internet.

##  Instal·lació d'OpenWrt
El procés per instal·lar OpenWrt al dispositiu es troba documentat a la corresponent pàgina de la [Wiki d'OpenWrt](https://openwrt.org/toh/ubiquiti/ubiquiti_edgerouter_x_er-x_ka "OpenWrt Wiki EdgeRouter X-SFP (ER-X-SFP)").
No és un procés trivial però tota la informació necessària es troba alla, per això no cal repetir-la aquí.

### OpenWrt 18.06
Farem servir la versió d'[OpenWrt 18.06](https://openwrt.org/releases/18.06/start "OpenWrt 18.06"), que és la *versió estable* a l'hora d'escriure aquest document (tardor 2018).
En particular, emprarem la darrera *service release* d'[OpenWrt 18.06.1](https://openwrt.org/releases/18.06/notes-18.06.1 "OpenWrt 18.06.1").

## Procés de configuració
### Establiment d'una contrasenya
Per defecte, OpenWrt s'instal·la sense cap contrasenya.
El primer que cal fer, doncs, és connectar-se a la interfície web del router i canviar-la.
Connectem l'ordinador a un dels ports de la `LAN` (que estan rotulats al dispositiu com `eth1`, `eth2`, `eth3`, `eth4`), entrem a la interfície web via [http://192.168.1.1](http://192.168.1.1) i canviem la contrasenya.

### Canvi de nom del dispositiu
Sempre val la pena canviar el nom per defecte, OpenWrt, pel que haguem declarat a la web de Guifi.net (UPC-CN-C5-ER-X-SFP).

### Configuració adreça IP de la interfície LAN
Canviem l'adreçament de la interfície `LAN`, d'acord amb les [dades de la configuració de xarxa](#sss-dades-xarxa) de més amunt:

`Protocol`: `Static address`  
`IPv4 address`: `10.1.24.1`  
`IPv4 netmask`: `255.255.255.224`  

Apliquem els canvis i a partir d'ara ens connectem a la interfície web a través d'[http://10.1.24.1](http://10.1.24.1).

### Connexió a Internet de la interfície WAN
La interfície lògica `WAN` està assignada, per defecte, al port rotulat com `eth0`.
Connectem el cable que proporciona connexió a Internet al port `eth0` del router.

### Instal·lació de paquets addicionals
A través de la interfície web o pel terminal, amb SSH, actualitzem la llista de paquets i n'instal·lem alguns més:
```bash
opkg update;
opkg install luci-ssl iperf3 curl \
kmod-i2c-gpio-custom kmod-gpio-pca953x \
bmx6 bmx6-json bmx6-sms bmx6-table bmx6-uci-config luci-app-bmx6 \
bird4 bird4-uci luci-app-bird4 \
luci-proto-ppp xl2tpd
```

### Activació del PoE
El router que fem servir permet alimentar mitjançant [PoE](https://ca.wikipedia.org/wiki/Power_over_Ethernet "PoE a la Viquipèdia") els dispositius que siguin compatibles a través dels ports `eth0`-`eth4`.
Per fer-ho cal instal·lar alguns paquets, com hem fet al pas anterior, i afegir el següent contingut al fitxer `/etc/rc.local`:

```bash
# Carreguem el mòdul GPIO I²C i activem el controlador del PoE
insmod i2c-gpio-custom bus0=0,3,4
echo pca9555 0x25 >/sys/bus/i2c/devices/i2c-0/new_device

# Inicialitzem tots els ports (eth0, eth1, eth2, eth3 i eth4).
#
# Apagat:
#   echo "0" > /sys/class/gpio/gpioXXX/value
#
# Activat:
#   echo "1" > /sys/class/gpio/gpioXXX/value
#
# Port "eth0" (WAN)
echo "496" > /sys/class/gpio/export
echo "out" > /sys/class/gpio/gpio496/direction
echo "0" > /sys/class/gpio/gpio496/value
# Port "eth1" (LAN)
echo "497" > /sys/class/gpio/export
echo "out" > /sys/class/gpio/gpio497/direction
echo "0" > /sys/class/gpio/gpio497/value
# Port "eth2" (LAN)
echo "498" > /sys/class/gpio/export
echo "out" > /sys/class/gpio/gpio498/direction
echo "0" > /sys/class/gpio/gpio498/value
# Port "eth3" (LAN)
echo "499" > /sys/class/gpio/export
echo "out" > /sys/class/gpio/gpio499/direction
echo "0" > /sys/class/gpio/gpio499/value
# Port "eth4" (LAN)
echo "500" > /sys/class/gpio/export
echo "out" > /sys/class/gpio/gpio500/direction
echo "0" > /sys/class/gpio/gpio500/value

exit 0
```

Al codi d'exemple de dalt tots els ports tenen el PoE desactivat.
Per al node que estem configurant, l'activarem pels ports `eth1` i `eth2`.

<div class="alert alert-warning">
Cal vigilar de **no connectar dispositius que no suporten PoE als ports que el tenen activat**, ja que **es faran malbé**
</div>

### Configuració de BMX6
Configurem BMX6 manualment, per tal que faci mesh amb la resta de dispositius del node per cable, i encamini paquets amb la resta de la mesh.

#### Plugins
A la interfície web anem a *Network* → *BMX6* ⟶ *Configuration* → *Plugins* i afegim tots els *plugins*: `bmx6_config.so`, `bmx6_json.so`, `bmx6_sms.so` i `bmx6_table.so`.

Desem i apliquem els canvis.

#### Interfícies de xarxa
A la interfície web anem a *Network* → *BMX6* ⟶ *Configuration* → *General* i afegim la interfície `br-lan` (que correspon als ports físics marcats com `eth1`, `eth2`, `eth3` i `eth4`).
Si hi ha alguna altra interfície activada, com `lo`, podem eliminar-la.

Desem i apliquem els canvis.

Per filar prim, anem a *Network* → *BMX6* ⟶ *Configuration* → *Interfícies* i configurem el paràmetre `linklayer` la interfície amb el valor `1` (és a dir, li diem que és cablejada)

`Device`: `br-lan`  
`linklayer`: `1`

Desem i apliquem els canvis.

#### Dispositiu túnel principal
A la interfície web anem a *Network* → *BMX6* ⟶ *Configuration* → *General* i configurem el dispositiu de túnel principal (Tunnel device):

`Name`: `tmain`  
`IPv4 address/length`: `10.1.24.1/27`  
`IPv6 address/length`: `2012::0:10:1:24:1:1/64`

Desem i apliquem els canvis.

#### Importar rutes IPv4 de la mesh
Volem que al router li arribin les rutes (quina redundància!) que anuncien la resta de nodes de la mesh.
A la interfície web anem a *Network* → *BMX6* ⟶ *Configuration* → *Tunnels* i configurem les xarxes que volem rebre (*Networks to fetch*):

`Name`: `cloud`  
`Network to fetch`: `10.0.0.0/8`  
`minPrefixLen`: `24`

`Name`: `cloud6`  
`Network to fetch`: `::/0`  
`minPrefixLen`: `48`

`Name`: `community`  
`Network to fetch`: `10.0.0.0/8`  
`maxPrefixLen`: `8`

`Name`: `community6`  
`Network to fetch`: `::/0`  
`minPrefixLen`: `32`  
`maxPrefixLen`: `48`

Desem i apliquem els canvis.

#### Exportar Guifi.net a la mesh
Estem configurant un router frontera, que anunciarà a Guifi.net les rutes de la mesh i a la mesh les de Guifi.net.
Com que injectar les ~3000 rutes de Guifi.net a la mesh faria petar BMX6 als nodes mesh (!!!), anunciarem a la mesh que tenim una passarel·la cap a `10.0.0.0/8`.
A la interfície web anem a *Network* → *BMX6* ⟶ *Configuration* → *Tunnels* i configurem la xarxa que volem publicar (*Networks to offer*):

`Name`: `Guifi`  
`Network to offer`: `10.0.0.0/8`  
`bandwidth`: `36`

De moment posem el paràmetre `bandwidth` al mínim (36), fins que no tinguem tota la part de BGP i Bird4 llesta, així cap router de la mesh l'agafarà.

Desem i apliquem els canvis.

#### Configuració dels \[altres\] dispositius qMp del node
A la interfície web dels dispositius amb qMp, tindrem cura de configurar-los per tal que no utilitzin VLANs per fer mesh.
Anirem a *Device configuration* → *Network settings* ⟶ *Wired interfaces* i veurem a través de quines interfícies estem fent mesh (típcament seran `eth0`, `eth1`, `eth0.1`, `eth0.2`).
En prendrem nota i, acte seguit, anirem a *Device configuration* → *Network settings* ⟶ *Advanced settings* i les introduïrem a l'apartat "VLAN-untagged devices".
Per exemple:

`VLAN-untagged devices`: `eth0 eth1`

### Configuració del túnel L2TP
A la interfície web anem a *Network* → *Interfaces* ⟶ *Add new interface...* i creem el túnel L2TP:

`Name of the new interface`: `upc` (pot ser qualsevo altre nom)  
`Protocol`: `PPP`  
Fem clic al botó *Submit*

`Status`: **Device**: ppp-upc  
`Protocol`: `L2TP`  
Fem clic al botó *Switch protocol*  
`L2TP Server`: `a.b.c.d` (l'adreça IP pública del servidor L2TP)  
`PAP/CHAP username`: `usuari` (l'usuari que toqui)
`PAP/CHAP password`: `1234` (màxima seguretat)

A la pestanya `Firewall settings` afegim la interfície túnel a la zona `wan`:  
`Create / Assign firewall-zone`: `wan` (wan, wan6)

Desem i apliquem els canvis.

### Configuració de Bird4 i BGP

#### Funcions de Bird4
A la interfície web anem a *Network* → *Interfaces* → *Bird4* ⟶ *Functions* i creem un nou fitxer de funcions amb el següent contingut:

```
function match_guifi_prefix() {

	return net ~ [
		10.0.0.0/8{9,28}
	];
}

function match_mesh_upc_prefix() {

	return net ~ [
		10.1.24.0/21{22,32}
	];
}
```

#### Filtres de Bird4
A la interfície web anem a *Network* → *Interfaces* → *Bird4* ⟶ *Filters* i creem un nou fitxer de filtres amb el següent contingut:

```
filter ebgp_in {                           

        krt_prefsrc = 10.1.24.1;
        if match_guifi_prefix() then accept;
        else reject;
}                                          
                     
filter ebgp_out {

        if match_guifi_prefix() then accept;
        else reject;         

}
```

#### Protocol BGP amb Bird4
A la interfície web anem a *Network* → *Interfaces* → *Bird4* ⟶ *BGP Protocol* i creem una nova plantilla de BGP (`BGP Templates`) anomenada `common` (sembla que **cal** que es digui així):

`Disabled`: `[ ]`  
`Local AS`: `102133` (la *id* del router a la web de Guifi.net)

Un cop llesta la plantilla, creem una instància de BGP (`BGP Instances`) anomenada `upc` (o el que convingui):

`Disabled`: `[ ]`  
`Templates`: `common` (la plantilla que acabem de crear)  
`Neighbor IP Address`: `172.29.17.1` (aquesta IP ve determinada per la configuració del servidor de túnel L2TP)  
`Neighbor AS`: `40605` (en el cas d'aquest peer)  
`Import`: `filter ebgp_in`  
`Export`: `filter ebgp_out`

Desem i apliquem els canvis.

#### Configuració global de Bird4
A la interfície web anem a *Network* → *Interfaces* → *Bird4* ⟶ *Overview* i a `Global options` posem:

`Router ID`: `10.1.24.1`

Desem i apliquem els canvis.

A la mateixa pàgina, a `Tables configuration`, esborrem la taula `aux`, que apareix creada per defecte.

Desem i apliquem els canvis.

#### Configuració general dels protocols
A la interfície web anem a *Network* → *Interfaces* → *Bird4* ⟶ *General protocols* i a `Direct options` creem una nova secció anomenada `interface`:

`Disabled`: `[ ]`  
`Interfaces`: `"br-lan"` (amb les cometes!)

Desem i apliquem els canvis.

A la mateix pàgina, a `Kernel options`, hi hauria d'haver la instància per defecte `kernel1`, que deixarem com:

`Disabled`: `[ ]`  
`table`: (buit, de manera que desaparegui en desar)  
`import`: `all`  
`export`: `all`  
`scan_time`: `10`  
`kernel_table`: (buit, de manera que desaparegui en desar)  
`learn`: `[✓]`  
`persist`: `[ ]`  

Desem i apliquem els canvis.

### Configuració del firewall

A la interfície web anem a *Network* → *Firewall* ⟶ *General settings* i a la secció `General settings` deixem tot igual, excepte:

`Forward`: `accept`

A la mateixa pàgina, a `Zones`, deixen la zona `lan` tal com està i fem els canvis a la zona `wan`:

|   Name   | Zone | ⇒ |  Forwardings  | Input  | Output | Forward | Masquerading | MSS clamping |
|----------|------|---|---------------|--------|--------|---------|--------------|--------------|
| **lan**  | lan  | ⇒ | wan           | accept | accept | accept  |      [ ]     |      [ ]     |
| **wan**  | wan  | ⇒ | \[REJECT\]    | reject | accept | accept  |      [ ]     |      [ ]     |
