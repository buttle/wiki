### ¿Qué hardware necesito como mínimo para comenzar?


### ¿Necesito una antena y un router?
Depende de la proyección que quieras para tu punto de acceso:

* **¿Unicamente quieres conectarte tú?**  Necesitas ....
* **¿Quieres que se conecte toda tu comunidad de vecinos?** Necesitas ....
* **¿Quieres poder facilitar el acceso a la red guifi a toda tu zona?**  Necesitas .... 

### ¿Qué equipo/marca es el/la más recomendable para comenzar?

### ¿A dónde me tengo que conectar?

Tu nodo debe conectarse a un SN o Super-Nodo y tus dispositivos se conectarán a tu nodo mediante un AP.

### ¿Que es un Nodo?

Un nodo es el punto de conexión final de la red al que se le pueden conectar diferentes equipos para dar diferentes servicios. 
Por ejemplo: tu antena y router forman un nodo, tu conectas a la wifi que da ese router / AP o mediente cable de red. Además puedes conectar un servidor donde alojar una página web.

### ¿Que es un Super-Nodo?

Un SN o Super-Nodo es un nodo dedicado ampliar la rama principal de la red guifi. Requiere una instalación y configuración mas amplia ya que su enlace debe ser mucho mas estable que un nodo.

### ¿Que es un AP?

AP es la abreviatura de "Access Point" en inglés o "Punto de acceso" en Español. Se hace referencia al punto de acceso que se utiliza para conectar los diferentes dispositivos finales (Móvil, tablet, Ordenador, etc)

### ¿Qué significan los puntos y estrellas del mapa de conexión?

Para empezar, decir que el mapa actual solo refleja las conexiones de tipo o modo infraestructura, es decir, conexiones entre supernodos a través de protocolos de enrutamiento BGP o OSPF, y de relación entre punto de acceso y estación. Otras tecnologías como la mesh, no se visualizan bien.

Los colores significan:

* Línea Azul: Enlace proyectado (pendiente)
* Línea Naranja: Enlace en pruebas (si funciona bien por un tiempo pasará a amarillo o verde)
* Línea Amarillo: Enlace de acceso a un cliente (nodo cliente conecta a supernodo), no extiende red
* Línea Verde: Enlace troncal, entre supernodos, enlace que extiende la red
* Punto Rojo: nodo mesh, los nodos mesh se está investigando mejoras de su visualización, se ven mejor aquí: http://sants.guifi.net/maps/ y http://libremap.net/

Los simbolos significan:

* Puntos: nodo, ubicación geográfica donde hay una instalación de telecomunicaciones guifi
* Lineas: conexiones entre nodos guifi (los puntos), pueden ser wifi, fibra óptica, cable, túnel, etc. Las lineas, en el caso de los cables, de momento no dicen por dónde pasan.
* Estrellas: se visualiza de esta manera todo nodo/ubicación/punto que tenga más de un trasto/cacharro/dispositivo

### ¿El SN al que me conecto debe estar apuntando hacia mi antena?

Cuanto mayor sea la alinación entre los dos puntos mejor será la señal. Normalmente se instalan antenas omnidireccionales para dar una señal de 360º

### ¿Cómo averiguo si el nodo que está cerca de mí encara hacia mi antena y puede darme una buena señal?

Se puede hacer por aproximaciones. Si te guías por alguien que ya forma parte de guifi o tiene experiencia, la aproximación será mejor. Especialmente, compartir esta información con quien vas a conectar puede ayudar mucho.

1. La más sencilla: hay visibilidad directa entre los nodos.
2. Comprobar que las elevaciones son apropiadas para las zonas fresnel con esta aplicación. Alerta, no tiene en cuenta edificios, solo terreno. Lo cual lo hace más fiable en entorno rural http://wisp.heywhatsthat.com/
3. Hacer una prueba de cobertura: comprar una antena, o preguntar en tu comunidad si hay disponible una antena para hacer pruebas, o que un voluntario o instalador nos ayude. Cuanto más realista sea la prueba, mejor aproximación será. Necesitaremos un palo y conexión eléctrica allí donde haremos la prueba. Por ejemplo, durante esta prueba usar la conexión con el uso previsto a ver qué tal funciona. Y tratar de ponerla en la ubicación donde sería la instalación definitiva. Esta es la prueba más fiable, si funciona bien, la instalación final también.

### Qué frecuencia es mejor ¿2.4 GHz o 5 GHz?

Actualmente las ciudades estan saturadas de la frecuencia 2.4 GHz por lo que se recomienda usar 5 GHz

### ¿Puedo conectar mi antena de 5 Ghz a un nodo de 2.4 Ghz o al revés?

No. Antes de comprar el equipo debes tener bien claro a que SN vas a conectarte y a que frecuencia trabaja. Hay antenas que son de doble banda (2.4 y 5 GHz) pero es improbable que la tuya lo sea.

### ¿Cómo averiguo el tipo de antena que tiene y a que frequencia trabaja el nodo al que quiero conectarme?

La aplicación de guifi.net es en sí una base de datos gelocalizada que da información sobre sus nodos y sus dispositivos, y sobre las personas que los gestionan. Adicionalmente, otras herramientas pueden ser el contacto con sus administradores vía correo electrónico, chat, listas de correo, foro, en persona, etc.

### ¿Que puede hacer un instalador por mí?

En pocas palabras: garantizar que la instalación va a salir bien. Esto es especialmente importante si el colectivo que gestiona el espacio quiere depositar la confianza de la instalación en alguien que presuntamente lo va a ejecutar de forma profesional y que va a aportar a través de su experiencia, certificados, responsabilidad, reputación, etc.

### Qué diferencia hay entre los paradigmas mesh e infraestructura

Infraestructura recibe su nombre por su propio modo de operación llamado [infraestructura](https://en.wikipedia.org/wiki/Wireless_LAN#Infrastructure). En este modo hay dos roles: [estación](https://en.wikipedia.org/wiki/Station_(networking)) (cliente) y [punto de acceso](https://en.wikipedia.org/wiki/Wireless_access_point) (servidor). En terminología de guifi se le llama **supernodo** a la infraestructura de telecomunicaciones que extiende la red de guifi, y para ello se conecta a otros supernodos. Así pues, tiene más de un enlace y normalmente está conformado por varias radios externas y un router; lo que se conoce como un [supernodo híbrido](http://blackhold.nusepas.com/2011/04/09/nodos-hibridos-guifi-net/), [otra referencia que explica supernodos híbridos](tombatossals.github.io/taller-nodos-hibridos/). Un **nodo** se conforma normalmente de un dispositivo exterior (por ejemplo en la azotea); y lo es si es una infraestructura fija que se conecta a un supernodo. Por lo tanto un portátil (o una antena) que se conecta esporádicamente a un supernodo no es un nodo. Por su naturaleza pasiva, un nodo no extiende la red de guifi y depende de un supernodo.

Mesh tiene su nombre por el uso del [modo ad-hoc](https://en.wikipedia.org/wiki/Wireless_LAN#Peer-to-peer) y/o [802.11s](https://en.wikipedia.org/wiki/IEEE_802.11s) que se pueden entender como modos que permiten una conexión tipo P2P (entre pares / horizontal). Por lo tanto la jerarquía es más plana, todos los nodos ayudan a extender la red, por lo tanto la distinción es más suave, simplemente entre nodos más o menos importantes (algunos parámetros son: las buenas vistas del emplazamiento y la infraestructura y servicios que contiene).

Tanto un paradigma como otro son capaces de desplegar una red de telecomunicaciones de alta calidad, es como lo que se suele decir que [con cualquier lenguaje de programación lo puedes hacer todo](https://en.wikipedia.org/wiki/Turing_completeness). Lo que pasa que la naturaleza de cada paradigma trae consigo diferentes pros y contras.

La terminología es comunmente usada pero incorrecta desde un punto de vista académico. Tanto el modo mesh como infraestructura son "mesh" (pueden mallarse, tener redundancias); si bien es cierto que el modo mesh es más propenso a mallarse mucho más que el modo infraestructura.

| Property                | Mesh                            | Insfrastructure  |
|-------------------------|---------------------------------|------------------|
| Deployed IP version     | IPv4, IPv6                      | IPv4             |
| Firmware license        | Free open source                | Mostly privative |
| Routing protocol        | bmx6                            | BGP, OSPF        |
| Designed for wifi links | Yes                             | No               |
| Average deployment time | Shorter                         | Longer           |
| Average cost per link   | Less                            | More             |
| Link quality            | Low                             | High             |
| Common operations       | User level                      | Technical level  |
| Uncommon operations     | Technical and Programming level | Technical level  |

- nubes de conceptos relacionados con las tecnologías. Es decir, términos que se usan más cuando se está hablando de un paradigma o de otro
    - nube de conceptos relacionados con mesh: Linux, [GPL](https://en.wikipedia.org/wiki/GNU_General_Public_License), OpenWRT, battlemesh.org, firmwares libres, libremesh.org, [temba](https://gitlab.com/guifi-exo/temba), qmp.cat, protocolos de d'enrutamiento libres, bmx6, bmx7, batman-adv (advance), batman, manet (Wireless Mesh Networking)
    - nube de conceptos relacionados con infraestructura: BGP, *baixo o pujo el* [Peer](https://wiki.mikrotik.com/wiki/Manual:Routing/BGP#Peer), OSPF, Mikrotik, Ubiquiti, firmware de fábrica / [OEM](https://en.wikipedia.org/wiki/Original_equipment_manufacturer), RouterOS, [winbox](https://wiki.mikrotik.com/wiki/Manual:Winbox), AirOS, [WDS](https://en.wikipedia.org/wiki/Wireless_distribution_system), [CCQ](https://wiki.mikrotik.com/wiki/Manual:Wireless_FAQ#What_is_CCQ_and_how_are_the_values_determined.3F)
